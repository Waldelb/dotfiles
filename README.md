## What is this?
A config for zsh, that aims to make it as comfortable as the fish-shell.
Why not use fish instead? Because fish does not support POSIX, which makes it complicated to copy commands from guides.

## Usage
- Install dependencies (for example on Arch: `sudo pacman -S zsh zsh-syntax-highlighting zsh-autosuggestions zsh-history-substring-search pkgfile`)
- download the `.zshrc` file, and copy it into your home directory (`/home/<your_username>/` while substituting <your_username> with your username)

## Authors and acknowledgment
Forked from https://github.com/Chrysostomus/manjaro-zsh-config

## License
MIT License

Copyright (c) 2017 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
